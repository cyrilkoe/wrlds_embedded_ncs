/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT invensense_icm20948

#include <device.h>
#include <drivers/i2c.h>
#include <kernel.h>
#include <drivers/sensor.h>
#include <sys/__assert.h>
#include <logging/log.h>

#include "icm20948.h"
#include "icm20948_api.h"

LOG_MODULE_REGISTER(ICM20948, CONFIG_SENSOR_LOG_LEVEL);

const struct device *i2c_master;

int8_t icm20948_i2c_read(const uint8_t reg_addr, uint8_t *data, const uint32_t len)
{
	return i2c_burst_read(i2c_master, ICM20948_I2C_ADDR, reg_addr, data, len);
}

int8_t icm20948_i2c_write(const uint8_t reg_addr, uint8_t *data, const uint32_t len)
{
	return i2c_burst_write(i2c_master, ICM20948_I2C_ADDR, reg_addr, data, len);
}

void icm20948_delay_us(uint32_t period)
{
	k_usleep(period);
}

static int icm20948_sample_fetch(const struct device *dev,
			     enum sensor_channel chan)
{
	struct icm20948_data *drv_data = dev->data;

	icm20948_api_return_code_t ret;
	icm20948_api_accel_t accel_read;

	ret = icm20948_api_getAccelData(&accel_read);
	if(ret != ICM20948_RET_OK) {
		return ret;
	}

	drv_data->accel_x = accel_read.x;
	drv_data->accel_y = accel_read.y;
	drv_data->accel_z = accel_read.z;

	return 0;
}

static int icm20948_channel_get(const struct device *dev,
				enum sensor_channel chan,
				struct sensor_value *val)
{
	const struct icm20948_data *drv_data = dev->data;

	icm20948_api_accel_t accel_read;
	icm20948_api_return_code_t ret;

	switch (chan) {
	case SENSOR_CHAN_ACCEL_X:
		val->val1 = drv_data->accel_x;
		break;
	default:
		return -ENOTSUP;
	}

	return 0;
}

static const struct sensor_driver_api icm20948_driver_api = {
	.sample_fetch = icm20948_sample_fetch,
	.channel_get = icm20948_channel_get,
};

static int icm20948_init(const struct device *dev)
{
	struct icm20948_data *drv_data = dev->data;

	drv_data->i2c = device_get_binding(DT_INST_BUS_LABEL(0));
	if (drv_data->i2c == NULL) {
		LOG_ERR("Failed to get pointer to %s device!",
			    DT_INST_BUS_LABEL(0));
		return -EINVAL;
	}

	i2c_master = drv_data->i2c;

	uint8_t chip_id;

	if(i2c_reg_read_byte(drv_data->i2c, ICM20948_I2C_ADDR, ICM20948_REG_WHO_AM_I, &chip_id) < 0)
	{
		LOG_ERR("!!! Failed reading chip id !!!");
	}

	LOG_INF("ICM20948 device ID = %x\n", chip_id);

	icm20948_api_return_code_t ret = ICM20948_RET_OK;
	ret = icm20948_api_init((icm20948_api_read_fptr_t) &icm20948_i2c_read, (icm20948_api_write_fptr_t) &icm20948_i2c_write, (icm20948_api_delay_us_fptr_t) &icm20948_delay_us);

	if(ret != ICM20948_RET_OK)
	{
		LOG_INF("Error initializing api, code = %x\n", ret);
		return ret;
	}

	icm20948_api_settings_t settings;

	if(ret == ICM20948_RET_OK) {
        // Enable the Accel
        settings.accel.en = ICM20948_MOD_ENABLED;
        // Select the +-2G range
        settings.accel.fs = ICM20948_ACCEL_FS_SEL_2G;
        ret = icm20948_api_applySettings(&settings);
		if(ret != ICM20948_RET_OK)
		{
			LOG_INF("Error setting up api, code = %x\n", ret);
			return ret;
		}

    }

	return 0;
}

struct icm20948_data icm20948_driver;

DEVICE_DT_INST_DEFINE(0, icm20948_init, NULL,
		    &icm20948_driver, NULL,
		    POST_KERNEL, CONFIG_SENSOR_INIT_PRIORITY,
		    &icm20948_driver_api);
