import random
import numpy as np


# Run this script and copy the result in your xxx_clusters.h
# On ubuntu and for berg for instance : python3 gen_clusters.py > ../apps/sensor_test/src/berg_clusters.h


# Constants definition
num_windows = 6
window_size = 40
num_clusters = 1
cluster_distance_threshold = 0.1
rounding_parameter = 5

# Generate a complete .h as a string
def print_clusters_h(cluster_array=None):
    if(cluster_array == None):
        cluster_array = np.random.rand(num_clusters, num_windows*window_size)
    
    # Holds the total .h file
    str_result = ""

    # Holds the cluster array line (last line)
    str_cluster_array = "const windows_t * const cluster_array[] = {"

    # Preprocessor macros
    str_result += '#ifndef BERG_CLUSTERS_H__' + '\n'
    str_result += '#define BERG_CLUSTERS_H__' + '\n'
    str_result += '#include "berg_windows.h"' + '\n'
    str_result += '#define NUM_CLUSTERS '+str(num_clusters) + '\n'
    str_result += '#define CLUSTER_DISTANCE_THRESHOLD '+str(round(cluster_distance_threshold, rounding_parameter)) + 'f '+'\n'

    # Create the windows constant for each cluster
    for k in range(num_clusters):            
        str_result += "const windows_t cluster_"+str(k)+"_content = {" + '\n'
        str_result += "    {"

        # Holds the data array of the windows
        str_content = ""
        for i in range(num_windows*window_size):
            str_content += str(round(cluster_array[k][i], rounding_parameter)) + ', '
        
        # Write the second parameter of the windows (the pointer array)
        str_result += str_content[:len(str_content)-2]
        str_result += "}," + '\n'
        str_result += "    {0}" + '\n'
        str_result += "    };" + '\n'

        # Write the address of our windows object in the cluster array for later
        str_cluster_array += "&cluster_"+str(k)+"_content, "

    # Clean the cluster array line
    str_cluster_array = str_cluster_array[:len(str_cluster_array)-2]
    str_cluster_array += "};"
    
    # Add it to the file
    str_result += str_cluster_array + '\n'
    # Add last line
    str_result += '#endif' + '\n'
    # Return the complete file
    return str_result

# Code generation without data (random)
print(print_clusters_h())

# Code generation if you have for instance two clusters :
if(False):
    cluster_array = np.array([0, 0, 0, 0],
                             [1, 1, 1, 1])
    print(print_clusters_h(cluster_array))