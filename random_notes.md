- If when you run the ML model (classify function) you observe that it blocks. Try pausing the debugger, if it blocks during a memory operation (free, calloc, malloc, ...) verify that you have the rights size of the input and the right number of channels !
- If the compilation seems to be weird clean your build folder : delete it and reopen the project on segger or rebuild on command line
- If nothing happends, and when you pause the debugger you see that you are blocked on MPSL_IRQ_CLOCK_HANDLER don't forget to add CONFIG_CLOCK_CONTROL_NRF_K32SRC_RC=y to your prj.conf

Problems with edge impulse on old sdk:
C++ compiler on segger impossible, with segger problems that some C++ pakages are not found. So i tried compiling the edgeimpulse-standalone-C with a makefile. Which could give me a library.a but still impossible to use.

with the vscode plugin we need to give the compile but the manually toolchain doesn't work so easily. So open nrf connect for desktop, toolchain manager and download the v 1.6.0 in C:/users/you_user/ncs

then open vscode and downlaod nrf connect extension pack

open the vscode workspace settings (wrlds_embedded_ncs/.vscode/workspace...) and modify the toolchain path with your toolchain path. 

now open the workspace in vsstudio with : file -> open workspace from file -> and choose (wrlds_embedded_ncs/.vscode/workspace...).

on the left bar you have access to 5 windows : explorer (find your source files and edit them), search (look for a keyword in all the files), source control (git, useless), run and debug (open automatically when you start a debug), extensions (downlad plugins), nrf connect (start a debug, build etc.)

if you have problems with building you can try opening a console, rm -r build and compile directly from the console. Later you may retry compiling it from the button in the nrf connect window

nrf connect quick setup in CTRL+MAL+P : search for nrf connect welcome and then inside add your sdk and toolchain