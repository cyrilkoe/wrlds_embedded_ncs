## Prepare you development environment
### Open VSCode
Add the folders `wrlds_embedded_ncs` (code) and `ncs` (sdk) to your workspace.

Open a terminal in VSCode in case

### Open segger
Start segger and click  `File -> Open NRF Connect SDK project`. Browse the project  `apps/sensor_test` and browse the board in `apps/sensor_test/boards/arm/52832_berg_v02`. Note that you can also put your custom board in `wrlds_embedded_ncs/board` and link them to the Zephyr directory or even directly put your custom board in the Zephyr directory so you don't need to browse each time. I recommand letting them in the `apps/sensor_test/boards/` since in general one board is used for one app.

### Take a look at CMakeLists.txt
You may need to modify it if you add new folders, new sourcefiles...

### Compile on command line
Open your windows powershell or ubuntu bash and run `west build nrf52832_berg_v02`, you can now flash with `west flash` or using your phone and the bluetooth DFU. Make sure that the line `list(APPEND BOARD_ROOT ${CMAKE_CURRENT_SOURCE_DIR})` is in your `CMakeLists.txt` to be able to find the board

<br/><hr><br/>

## Update the ML model

When you need to update the ML Model just remove `edge-impulse-sdk` , `tflite-model` and `model-parameters` and replace them by your new version. \
ALWAYS VERIFY THE SIZE OF YOUR WINDOWS ETC : You can find the edge constants in `model-parameters` and the app constants in `berg_windows.h`

<br/><hr><br/>

## Update the clusters

Give a look in `wrlds_embedded_ncs\scripts\gen_clusters.py`. This script generates clusters randomly or generate your own clusters from a 2D numpy array using the same file

<br/><hr><br/>

## Use the DFU

To prepare a new version of the firmware and upload it with the DFU :

First compile the project, or in 

<br/><hr><br/>