cmake_minimum_required(VERSION 3.13.1)

# Set cflags
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

#set(ZEPHYR_TOOLCHAIN_VARIANT gnuarmemb)
#set(GNUARMEMB_TOOLCHAIN_PATH C:/Users/cyril/Documents/wrlds/gnuarmemb)

# Tell the compiler to find the board definition here
list(APPEND BOARD_ROOT ${CMAKE_CURRENT_SOURCE_DIR})

# Find zephyr OS
find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})

# Define project
project(example-standalone-inferencing-zephyr)

# Set colorful output
zephyr_compile_options(-fdiagnostics-color=always)

# Import usefull cmake macros definitions
include(utils/cmake/utils.cmake)

# (Edge Impulse) Use hardware acceleration for DSP and Neural Network code
# You'll need to disable these on non-Arm cores
add_definitions(-DEIDSP_USE_CMSIS_DSP=1
                -DEI_CLASSIFIER_TFLITE_ENABLE_CMSIS_NN=1
                -DARM_MATH_LOOPUNROLL
                )

# (Edge Impulse) add the SDK
add_subdirectory(edge-impulse-sdk/cmake/zephyr)

# Include directories
set(INCLUDES
    .
    src
    tflite-model
    model-parameters
    edge-impulse-sdk
    )
include_directories(${INCLUDES})

# (Edge Impulse) add the compiled model
RECURSIVE_FIND_FILE(MODEL_FILES "tflite-model" "*.cpp")
list(APPEND SOURCE_FILES ${MODEL_FILES})

# Add our src folder
RECURSIVE_FIND_FILE(APP_CPP_FILES "src" "*.cpp")
RECURSIVE_FIND_FILE(APP_C_FILES "src" "*.c")
list(APPEND SOURCE_FILES ${APP_CPP_FILES})
list(APPEND SOURCE_FILES ${APP_C_FILES})

# Add sources as targets
target_sources(app PRIVATE ${SOURCE_FILES})