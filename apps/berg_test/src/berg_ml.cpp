
#include "berg_ml.hpp"

extern "C" {
#include "berg_windows.h"
}

#include "edge-impulse-sdk/classifier/ei_run_classifier.h"
#include "edge-impulse-sdk/dsp/numpy.hpp"

#include <math.h>

////////////////////////////////////////
///// CLUSTERS DEFINITIONS /////////////
////////////////////////////////////////

// <!> Make sure the end of the cluster file contains the cluster_array definition and the NUM_CLUSTERS preprocessing definition see /scripts/gen_random_clusters.py
#include "berg_clusters.h"

////////////////////////////////////////
///// PREPROCESSING FUNCTIONS  /////////
////////////////////////////////////////

// Copy the content of the raw sensor windows inside the ML windows
int fill_ml_windows() {
  int num_writes = 0;
  // For each type of sensor window
  for (int window_idx = 0; window_idx < NUMBER_RAW_WINDOWS; window_idx++) {
    // While the raw window is ahead
    while (ml_windows->write_ptrs[window_idx] != raw_windows->write_ptrs[window_idx]) {
      // Get the value in the raw window and normalize it
      float value = raw_windows->data[ml_windows->write_ptrs[window_idx]];
      if (window_idx < WIN_GYR_X) {
        value /= 2000.0; // Here we are reading an accelerometer value
      } else {
        value /= 16.0; // Here we are reading a gyroscope value
      }
      // Write the normalized value in the ml window
      write_windows_t(ml_windows, (window_index_t)window_idx, value);
      num_writes++; // Return to the main the number of updates made (used to know when to redo the classification)
    }
  }
  return num_writes;
}

// Returns the euclidian distance between two arrays of size 'size'
float compute_distance(const float *arr_1, const float *arr_2, uint16_t size) {
  float result = 0;
  for (uint16_t i = 0; i < size; i++) {
    result += pow(arr_1[i] - arr_2[i], 2);
  }
  return sqrt(result);
}

// Compare the ML windows with the clusters windows
int find_best_cluster() {
  float thresold = CLUSTER_DISTANCE_THRESHOLD;
  int result = -1;
  for (unsigned int i = 0; i < NUM_CLUSTERS; i++) {
    float distance = compute_distance(&(ml_windows->data[0]), &(cluster_array[i]->data[0]), TOTAL_WINDOW_SIZE);
    if (distance < thresold) {
      result = i;
      break;
    }
  }
  return result;
}

////////////////////////////////////////
///// EDGE IMPLUSE PART ////////////////
////////////////////////////////////////

// Helper function for edge impulse to retrieve our input data
int ml_feature_get_data(size_t offset, size_t length, float *out_ptr) {
  memcpy(out_ptr, &(ml_windows->data[offset]), length * sizeof(float));
  return 0;
}

uint8_t classify() {
  int64_t start_time_ms = k_uptime_get();
  uint8_t best_cluster = find_best_cluster();

  ei_impulse_result_t result = {0};

  // the features are stored into flash, and we don't want to load everything into RAM
  signal_t features_signal;
  features_signal.total_length = TOTAL_WINDOW_SIZE;
  features_signal.get_data = &ml_feature_get_data;

  // invoke the impulse
  EI_IMPULSE_ERROR res = run_classifier(&features_signal, &result, false);

  if (res != 0)
    return -1;

  float max_classification_value = 0.0f;
  uint8_t max_classification_idx = 0;

  for (size_t ix = 0; ix < EI_CLASSIFIER_LABEL_COUNT; ix++) {
    printk("%c %f ,", result.classification[ix].label[0], result.classification[ix].value);
    if (result.classification[ix].value > max_classification_value) {
      max_classification_value = result.classification[ix].value;
      max_classification_idx = ix;
    }
  }
  printk("\nResult : %i\n", max_classification_idx);
  int64_t total_delay_ms = k_uptime_get() - start_time_ms;
  printk("%u ms, %u ms, %u ms\n", result.timing.classification, result.timing.dsp, total_delay_ms);
  return max_classification_idx;
}