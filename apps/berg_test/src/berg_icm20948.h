#ifndef BERG_ICM20948_H__
#define BERG_ICM20948_H__

#include <zephyr.h>

// Init accelerometer
int icm20948_init(uint32_t acc_period_ms, uint32_t gyr_period_m);

// Ask for reading the FIFO of the accelerometer
void icm20648_read_values();

#endif