extern "C"
{
#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <drivers/sensor.h>
#include "berg_bluetooth.h"
#include "berg_i2c.h"
#include "berg_icm20948.h"
#include "berg_windows.h"
}

#include "berg_ml.hpp"


/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS 50

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)
#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0_LABEL DT_GPIO_LABEL(LED0_NODE, gpios)
#define LED0_PIN DT_GPIO_PIN(LED0_NODE, gpios)
#define LED0_FLAGS DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0_LABEL ""
#define LED0_PIN 0
#define LED0_FLAGS 0
#endif

/* The devicetree node identifier for the "led1" alias. */
#define LED1_NODE DT_ALIAS(led1)
#if DT_NODE_HAS_STATUS(LED1_NODE, okay)
#define LED1_LABEL DT_GPIO_LABEL(LED1_NODE, gpios)
#define LED1_PIN DT_GPIO_PIN(LED1_NODE, gpios)
#define LED1_FLAGS DT_GPIO_FLAGS(LED1_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led1 devicetree alias is not defined"
#define LED1_LABEL ""
#define LED1_PIN 0
#define LED1_FLAGS 0
#endif

/* The devicetree node identifier for the "led2" alias. */
#define LED2_NODE DT_ALIAS(led2)
#if DT_NODE_HAS_STATUS(LED2_NODE, okay)
#define LED2_LABEL DT_GPIO_LABEL(LED2_NODE, gpios)
#define LED2_PIN DT_GPIO_PIN(LED2_NODE, gpios)
#define LED2_FLAGS DT_GPIO_FLAGS(LED2_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led2 devicetree alias is not defined"
#define LED2_LABEL ""
#define LED2_PIN 0
#define LED2_FLAGS 0
#endif


void main(void)
{
    const struct device *leds[3];
    const uint8_t pins[] = {LED0_PIN, LED1_PIN, LED2_PIN};
    bool led_is_on = false;
    int err_code;
    const uint32_t acc_period_ms = 20 /* 50 hz */, gyr_period_ms = 20 /* 50 hz */;

    // Start bluetooth
    berg_bluetooth_init();

    leds[0] = device_get_binding(LED0_LABEL);
    if (leds[0])
    {
        err_code = gpio_pin_configure(leds[0], LED0_PIN, GPIO_OUTPUT_ACTIVE | LED0_FLAGS);
        if (err_code)
        {
            printk("Led0 not configured\n");
        }
    }
    else
    {
        printk("Led0 not found\n");
    }

    leds[1] = device_get_binding(LED1_LABEL);
    if (leds[1])
    {
        err_code = gpio_pin_configure(leds[1], LED1_PIN, GPIO_OUTPUT_ACTIVE | LED1_FLAGS);
        if (err_code)
        {
            printk("Led1 not configured\n");
        }
    }
    else
    {
        printk("Led1 not found\n");
    }

    leds[2] = device_get_binding(LED2_LABEL);
    if (leds[2])
    {
        err_code = gpio_pin_configure(leds[2], LED2_PIN, GPIO_OUTPUT_ACTIVE | LED2_FLAGS);
        if (err_code)
        {
            printk("Led2 not configured\n");
        }
    }
    else
    {
        printk("Led2 not found\n");
    }

    gpio_pin_set(leds[0], LED0_PIN, (int)false);
    gpio_pin_set(leds[1], LED1_PIN, (int)false);
    gpio_pin_set(leds[2], LED2_PIN, (int)false);
    
    bool led_state = false;

    init_windows();

    err_code = i2c_init();
    if (err_code)
    {
        printk("I2C init error\n");
    }
    else
    {
        printk("I2C initialized\n");
    }

    k_msleep(50);

    err_code = icm20948_init(acc_period_ms, gyr_period_ms);
    if (err_code)
    {
        printk("ICM-20948 init error\n");
    }
    else
    {
        printk("ICM-20948 initialized\n");
    }

    k_msleep(50);

    int distance_window = 25; // After how many datapoints should we run the classifier
    int total_writes = 0;

    while (1)
    {
        icm20648_read_values();
        total_writes += fill_ml_windows();

        if(is_connected) {
            gpio_pin_set(leds[0], LED0_PIN, (int)false);
            gpio_pin_set(leds[1], LED1_PIN, (int)true);
        } else {
            gpio_pin_set(leds[0], LED0_PIN, (int)true);
            gpio_pin_set(leds[1], LED1_PIN, (int)false);
        }

        if(total_writes > distance_window * NUMBER_RAW_WINDOWS) {
            uint8_t label = classify();
            total_writes = 0;
            if(is_connected)
                hrs_notify(label);
        }

        k_msleep(50);
    }
}
