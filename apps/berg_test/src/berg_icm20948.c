#include "berg_icm20948.h"
#include "berg_i2c.h"
#include "berg_windows.h"
#include "berg_utils.h"

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>

#include "Invn/Devices/DeviceIcm20948.h"
#include "Invn/Devices/HostSerif.h"

#define ICM20948_I2C_ADDR 0x68

////////////////////////////////////////
///// GLOBAL VARIABLES /////////////////
////////////////////////////////////////

// ICM Device
static inv_device_t *device;

// List of sensors and sampling periods
typedef struct
{
    uint8_t type;
    uint32_t period_us;
} sensor_params_t;
static sensor_params_t sensor_list[10];

uint64_t available_sensor_mask = 0;

// Sliding window sent to ML preprocessing
//raw_windows_t raw_windows;

// Firmware image
static const uint8_t dmp3_image[] = {
#include "Invn/Images/icm20948_img.dmp3a.h"
};

// ICM device
static inv_device_icm20948_t device_icm20948;

// I2C master instance
static int icm20948_read(uint8_t reg, uint8_t *data, uint32_t len);
static int icm20948_write(uint8_t reg, uint8_t *data, uint32_t len);
static const inv_host_serif_t serif_instance_i2c = {
    0,                       // Open
    0,                       // Close
    icm20948_read,           // Read reg
    icm20948_write,          // Write reg
    0,                       // Interrupt callback
    1024 * 32,               // Max read size
    1024 * 32,               // Max write size
    INV_HOST_SERIF_TYPE_I2C, // Serif type
};

////////////////////////////////////////
///// INTERFACE FUNCTIONS //////////////
////////////////////////////////////////

// Read I2C function
static int icm20948_read(uint8_t reg, uint8_t *data, uint32_t len)
{
    i2c_read_bytes(reg, data, len, ICM20948_I2C_ADDR);
}

// Write I2C function
static int icm20948_write(uint8_t reg, uint8_t *data, uint32_t len)
{
    i2c_write_bytes(reg, data, len, ICM20948_I2C_ADDR);
}

// Sleep function (declared in Icm20948.h)
void inv_icm20948_sleep_us(int us)
{
    k_usleep(us);
}

// Get time function (declared in Icm20948.h)
uint64_t inv_icm20948_get_time_us()
{
    uint32_t time_ms = k_uptime_get_32();
    return (uint64_t) (1000 * time_ms);
}

////////////////////////////////////////
///// SENSOR CALLBACK FUNCTIONS ////////
////////////////////////////////////////

// Reading values callback
static void sensor_event_cb(const inv_sensor_event_t *event, void *arg)
{
    int err_code = 0;

    if (event->status == INV_SENSOR_STATUS_DATA_UPDATED)
    {
        switch (INV_SENSOR_ID_TO_TYPE(event->sensor))
        {
        case INV_SENSOR_TYPE_ACCELEROMETER:
            if(raw_windows->write_ptrs[WIN_ACC_X]%RAW_WINDOW_SIZE == 0)
            {
                //printk("acc : %f, %f, %f\n", event->data.acc.vect[0] * 1000, event->data.acc.vect[1] * 1000, event->data.acc.vect[2] * 1000);
            }
            write_windows_t(raw_windows, WIN_ACC_X, event->data.acc.vect[0] * 1000);
            write_windows_t(raw_windows, WIN_ACC_Y, event->data.acc.vect[1] * 1000);
            write_windows_t(raw_windows, WIN_ACC_Z, event->data.acc.vect[2] * 1000);
            break;

        case INV_SENSOR_TYPE_GYROSCOPE:
            if(NUMBER_RAW_WINDOWS < 6) {
                break;
            }
            if(raw_windows->write_ptrs[WIN_GYR_X]%RAW_WINDOW_SIZE == 0)
            {
                //printk("gyr : %f, %f, %f\n", event->data.gyr.vect[0], event->data.gyr.vect[1], event->data.gyr.vect[2]);
            }
            write_windows_t(raw_windows, WIN_GYR_X, event->data.gyr.vect[0]);
            write_windows_t(raw_windows, WIN_GYR_Y, event->data.gyr.vect[1]);
            write_windows_t(raw_windows, WIN_GYR_Z, event->data.gyr.vect[2]);
            break;
        default:
            break;
        }
    }
}

// ICM callback
static void sensor_event_cb(const inv_sensor_event_t *event, void *arg);
static const inv_sensor_listener_t sensor_listener = {
    sensor_event_cb, // Callback that will receive sensor events
    0                // Some pointer passed to the callback
};

// Ask for reading the FIFO of the accelerometer
void icm20648_read_values()
{
    inv_device_poll(device);
}

////////////////////////////////////////
///// INIT /////////////////////////////
////////////////////////////////////////

// Init ICM functio, set xxx_period_ms to disable the sensor xxx
int icm20948_init(uint32_t acc_period_ms, uint32_t gyr_period_ms)
{
    uint8_t whoami = 0xff;
    char array[100] = {0};

    // Translate the I2C bus in an Invensense bus
    int err_code = 0;
    if (serif_instance_i2c.open)
    {
        err_code = inv_host_serif_open(&serif_instance_i2c);
        CHECK(err_code);
    }

    inv_device_icm20948_init(&device_icm20948, &serif_instance_i2c, &sensor_listener, dmp3_image, sizeof(dmp3_image));

    // Get a simplified version of the ICM
    device = inv_device_icm20948_get_base(&device_icm20948);
    err_code = inv_device_whoami(device, &whoami);
    printk("WHOAMI CHECK : 0x%x\n", whoami);
    CHECK(err_code);

    // Init device
    err_code = inv_device_setup(device);
    if (err_code != 0)
    {
        printk("Failed: Setup device \n");
        CHECK(err_code);
    }

    // Update firmware
    err_code = inv_device_load(device, NULL, dmp3_image, sizeof(dmp3_image), true /* verify */, NULL);
    if (err_code != 0)
    {
        printk("Failed: Load DMP3 image \n");
        CHECK(err_code);
    }

    inv_fw_version_t fwversion;
    memset(&fwversion, 0, sizeof(inv_fw_version_t));
    inv_device_get_fw_info(device, &fwversion); // <!> This function is not implemented and does not return the fw version

    // Selection of sensors to use
    unsigned int num_sensors = 0;
    if (acc_period_ms > 0)
    {
        sensor_list[num_sensors++] = (sensor_params_t){INV_SENSOR_TYPE_ACCELEROMETER, acc_period_ms * 1000};
    }
    if (gyr_period_ms > 0)
    {
        sensor_list[num_sensors++] = (sensor_params_t){INV_SENSOR_TYPE_GYROSCOPE, acc_period_ms * 1000};
    }

    // Find available sensors
    available_sensor_mask = -1;
    for (unsigned i = 0; i < num_sensors; i++)
    {
        err_code = inv_device_ping_sensor(device, sensor_list[i].type);
        printk("Ping %s %s\n", inv_sensor_2str(sensor_list[i].type), (err_code == 0) ? "OK" : "KO");
        CHECK(err_code);
        available_sensor_mask |= (1ULL << sensor_list[i].type);
    }

    // Start available sensors
    for (unsigned i = 0; i < num_sensors; i++)
    {
        if (available_sensor_mask & (1ULL << sensor_list[i].type))
        {
            int err_code = inv_device_set_sensor_period_us(device, sensor_list[i].type, sensor_list[i].period_us);
            CHECK(err_code);
            err_code += inv_device_start_sensor(device, sensor_list[i].type);
            printk("Start %s %s\n", inv_sensor_2str(sensor_list[i].type), (err_code == 0) ? "OK" : "KO");
            CHECK(err_code);
        }
    }
    
    printk("available sensors : %llx\n", available_sensor_mask);
}