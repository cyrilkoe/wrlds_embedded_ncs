#ifndef BERG_I2C_H__
#define BERG_I2C_H__

#include <zephyr.h>

int i2c_init();
int i2c_write_bytes(uint8_t reg_addr, uint8_t *data, uint32_t num_bytes, uint16_t slave_addr);
int i2c_read_bytes(uint8_t reg_addr, uint8_t *data, uint32_t num_bytes, uint16_t slave_addr);

#endif