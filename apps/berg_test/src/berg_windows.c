#include "berg_windows.h"

#include <zephyr.h>


// Definition of the global windows used
windows_t *raw_windows, *ml_windows;

// Helper function to write a data in the right segment of a windows object
void write_windows_t(windows_t *windows, window_index_t window_idx, float value) {
    windows->data[ windows->write_ptrs[window_idx]++ ] = value;
    if(windows->write_ptrs[window_idx] == (window_idx+1) * RAW_WINDOW_SIZE) {
        windows->write_ptrs[window_idx] = window_idx * RAW_WINDOW_SIZE;
    }
}

// Helper function to read a data in the right segment of a windows object
void read_windows_t(windows_t *windows, window_index_t window_idx, uint16_t idx) {
    assert(window_idx < NUMBER_RAW_WINDOWS);
    return windows->data[ window_idx * RAW_WINDOW_SIZE + (idx % RAW_WINDOW_SIZE) ];
}


// Constructor of a window object
// <!> There is no destructor yet (useless in the current code), if you want to create temporary windows objects don't forget to free the memory
windows_t *new_windows_t() {
    windows_t *windows = (windows_t *) malloc(sizeof(windows_t));
    for(window_index_t i = 0; i < NUMBER_RAW_WINDOWS; i++) {
        // Init pointer
        windows->write_ptrs[i] = i * RAW_WINDOW_SIZE;
        // Fill with 0
        for(uint16_t j = 0; j < RAW_WINDOW_SIZE; j++) {
            write_windows_t(windows, i, 0.0f);
        }
    }
    return windows;
}

// Create the windows used in the firmware
void init_windows() {
    raw_windows = new_windows_t();
    ml_windows = new_windows_t();
    return;
}