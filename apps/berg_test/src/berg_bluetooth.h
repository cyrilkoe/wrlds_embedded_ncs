#ifndef DFU_TEST_BLUETOOTH_H__
#define DFU_TEST_BLUETOOTH_H__

#include <zephyr.h>

int berg_bluetooth_init(void);
void start_smp_bluetooth(void);
void hrs_notify(uint8_t value);

extern bool is_connected;

#endif