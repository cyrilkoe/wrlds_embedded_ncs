#ifndef BERG_ML_H__
#define BERG_ML_H__


extern "C" {
#include <zephyr.h>
#include "berg_windows.h"
}

extern const windows_t * const cluster_array[];

int fill_ml_windows();
uint8_t classify();
int find_best_cluster();

#endif // BERG_ML_H__