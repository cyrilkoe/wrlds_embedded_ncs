#include "berg_i2c.h"

#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/i2c.h>

/* The devicetree node identifier for the "led0" alias. */
#define MY_I2C DT_NODELABEL(i2c0)
#if DT_NODE_HAS_STATUS(MY_I2C, okay)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: i2c devicetree alias is not defined"
#endif

static struct device *i2c_dev = NULL;

int i2c_write_bytes(uint8_t reg_addr, uint8_t *data, uint32_t num_bytes, uint16_t slave_addr)
{
    //printk("writing %04u bytes of data to %2x in register %x\n", num_bytes, slave_addr, reg_addr);
    return i2c_burst_write(i2c_dev, slave_addr, reg_addr, data, num_bytes);
}

int i2c_read_bytes(uint8_t reg_addr, uint8_t *data, uint32_t num_bytes, uint16_t slave_addr)
{
    //uint8_t before = data[0];
    int err_code = i2c_burst_read(i2c_dev, slave_addr, reg_addr, data, num_bytes);
    //printk("[0x%2x] Reading %04u bytes from %x , first=%01x -> %01x\n", slave_addr, num_bytes, reg_addr, before, data[0]);
    return err_code;
}

int i2c_init()
{
    uint8_t cmp_data[16];
    uint8_t data[16];
    int i, ret;

    i2c_dev = device_get_binding(DT_LABEL(MY_I2C));
    if (!i2c_dev)
    {
        printk("I2C: Device driver not found.\n");
        return -1;
    }

    return 0;
}
