#ifndef BERG_WINDOWS_H__
#define BERG_WINDOWS_H__

#include <zephyr.h>

#define RAW_WINDOW_SIZE    40
#define NUMBER_RAW_WINDOWS 6
#define TOTAL_WINDOW_SIZE NUMBER_RAW_WINDOWS*RAW_WINDOW_SIZE

// Datatype of each window
typedef enum {WIN_ACC_X, WIN_ACC_Y, WIN_ACC_Z, WIN_GYR_X, WIN_GYR_Y, WIN_GYR_Z} window_index_t;

// Struct concatenating all the sensor window inside a large one
typedef struct {
    // Structure of the array : [acc_x, ..., acc_x, acc_y, ..., acc_y, acc_z, ..., acc_z, ...]
    float data[NUMBER_RAW_WINDOWS * RAW_WINDOW_SIZE];
    // Independants pointers to each of the segments (acc_x, acc_y, etc.)
    uint16_t write_ptrs[NUMBER_RAW_WINDOWS];
} windows_t;

// Declarations of the differents windows objects used
extern windows_t *raw_windows, *ml_windows;

// Init function to create the differents windows objects
void init_windows();

// Helper function to write a data in the right segment of a windows object
void write_windows_t(windows_t *windows, window_index_t window_idx, float value);

// Returns the value at the position idx of the window window_idx
void read_windows_t(windows_t *windows, window_index_t window_idx, uint16_t idx);


#endif //BERG_WINDOWS_H__