#include <zephyr.h>

#include "dfu_test_bluetooth.h"

void main(void)
{
	start_smp_bluetooth();

	/* The system work queue handles all incoming mcumgr requests.  Let the
	 * main thread idle while the mcumgr server runs.
	 */
	while (1) {
		k_sleep(K_MSEC(1000));
	}
}
